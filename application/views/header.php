<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width">
    <!-- <link rel="stylesheet" type="text/css" href="css/normalize.css" /> -->
    <!-- <link rel="stylesheet" type="text/css" href="css/fxsmall.css" /> -->
    <link rel="stylesheet" href="<?php echo $asset;?>/style.css">
    <script src="<?php echo $asset;?>/js/modernizr.custom.js"></script>
    <script src="<?php echo $asset;?>/js/jquery.js"></script>
    <script>
    function validasi(form){
        var valid = true;
                // var form = document.form_login;
                if (form.email.value=='') {
                    $('#login-name').html('*required');
                    valid = false;
                }else {
                    $('#login-name').html(''); 
                } 
                if (form.file.value=='') {
                    $('#login-pass').html('*required');
                    valid = false;
                }else {
                    $('#login-pass').html(''); 
                } 
                return valid;
            }
            </script>
        </head>
        <body>
            <div class="gotop"></div>
            <header id="header" class="site-header">
                <div class="container">
                    <div class="logo"><img src="<?php echo $asset;?>/img/dummy/logo.png" alt=""></div>
                    <div class="header-right">
                        <div class="searchbar">
                            <input type="search" class="search" placeholder="search">
                            <input type="submit" class="submit">
                        </div>
                    </div>
                </div>

            </header>
            <nav class="main-nav" style="height:auto;">
                <ul>
                    <li><a href="#" class="login_nav"></a></li>
                    <li><a href="<?php echo base_url();?>#home" class="home_nav"></a></li>
                    <li><a href="<?php echo base_url();?>#about" class="about_nav"></a></li>
                    <li><a href="<?php echo base_url();?>#stores" class="store_nav"></a></li>
                    <li><a href="<?php echo base_url();?>#bags" class="bags_nav"></a></li>
                    <li><a href="<?php echo base_url();?>#ask" class="ask_nav"></a></li>
                </ul>
            </nav>
            <div class="login-wrap">
                <div class="container">
                    <form action="<?php echo base_url();?>login/process"  method="POST" enctype="multipart/form-data" onSubmit="return validasi(this)"  name="form_login" id="form_login">
                        <div class="login-form" style="height:auto;">
                            <div class="control-group">
                                <input type="text" class="login-field" value="" placeholder="email" id="login-name" name="email" required>
                                <label class="login-field-icon fui-user" for="login-name"></label>
                            </div>

                            <div class="control-group">
                                <input type="password" class="login-field" value="" placeholder="password" id="login-pass" name="password" required>
                                <label class="login-field-icon fui-lock" for="login-pass"></label>
                            </div>
                            <p class="radioP">
                              <input type="radio" name="user_type" value="user" id="r1" required />
                              <label for="r1">
                                <span class="radioButtonGraph"></span>
                                USER
                            </label>
                        </p>
                        <p class="radioP">
                            <input type="radio" value="store" name="user_type" id="r2" required />
                          <label for="r2">
                            <span class="radioButtonGraph"></span>
                            STORE
                        </label>
                    </p>
                        <label class="login-field-icon fui-user" id="notif"></label>
                    <?php 
                    if($this->session->flashdata('status')=="error"){
                        ?>
                        <script>
                            $('.login-wrap').fadeToggle();
                            $('#notif').html("<b>Pastikan email dan password anda benar!</b>");
                            setTimeout(function() {
                                $('#notif').fadeOut();
                            }, 3000);
                            // alert("Login gagal. Pastikan email dan password anda benar!");
                        </script>
                        <?php
                    }
                    ?>
                    <input type="submit" class="btn btn-primary btn-large btn-block" value="login">
                    <!-- <a class="btn btn-primary btn-large btn-block" href="#">login</a> -->
                    <a class="login-link" href="#">Lost your password?</a>
                </div>
            </form>
        </div>

    </div>