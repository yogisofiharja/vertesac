<?php include 'header.php';?>
    <div id="content">
       <!--  <form action="<?php echo base_url();?>login/process"  method="POST" enctype="multipart/form-data" onSubmit="return validasi(this)"  name="form_login" id="form_login" style="margin-left:150px;margin-top:100px;color:#ffffff;">
            <label>Email</label><span id="e-email"></span><br>
            <input type="email" name="email" placeholder="email"/><br/><br/>
            <label>Password</label><span id="e-password"></span><br>
            <input type="password" name="password" placeholder="password"/><br/><br/>
            <input type="radio" name="user_type" value="user" checked="true">User
            <input type="radio" name="user_type" value="store">store<br/><br/>
            <input type="submit" value="login">
            <?php 
            if($this->session->flashdata('status')=="error"){
                echo "<h2>Login gagal. Pastikan email dan password anda benar!</h2>";
            }
            ?>
        </form> -->
        
        <div class="intro" id="home">
            <div class="container">
                <div class="inner">
                    <h3>welcome!</h3>
                    <h1>Vertesac in a nutshell</h1>
                    <p>Vertesac is a convergence product from fashion and information technology. We make a system into a bag, to make it smart enough to detect the usage history of the bag.</p>
                </div>
            </div>
        </div>
        <div class="info wow bla">
            <div class="container">
                <img src="<?php echo $asset;?>/img/Info.png" alt="">
            </div>
        </div>

        <div class="video">
            <div id="big-video">
                <!-- <iframe width="560" height="315" src="http://www.youtube.com/embed/fwjrfipOQ1Y" frameborder="0" allowfullscreen></iframe> -->
            </div>
        </div>

        <div class="about" id="about">
            <div class="container">
                <div class="inner">
                    <h2 class="section-title black wow rubberBand">about</h2>
                    <div class="col about-content">
                        <img src="<?php echo $asset;?>/img/dummy/about1.png" alt="">
                        <p>Tas yang fashionable dan harganya terjangkau,  praktis, bervariasi, dengan menjadi member berarti konsumen beraksi dalam kepedulian lingkungan juga!</p>
                    </div>
                    <div class="col about-content">
                        <img src="<?php echo $asset;?>/img/dummy/about2.png" alt="">
                        <p>Setelah membeli starter pack yang berisi smart shopping bag, dan user id + password, konsumen dapat menginstall mobile apps Vertesac pada smartphone mereka dan banyak lagi keuntungannya!</p>
                    </div>
                    <div class="col about-content">
                        <img src="<?php echo $asset;?>/img/dummy/about3.png" alt="">
                        <p>Mendapatkan promo khusus (diskon, poin, cashback) dari merchant-merchant yang bekerjasama dengan  Vertesac</p>
                    </div>
                </div>
                <a href="#" class="link">see how</a>
            </div>
        </div>
        <div class="slider">
            <div class="container">
                <div id="da-slider" class="da-slider">
                    <div class="da-slide">
                        <h2>Integrasi Sistem</h2>
                        <p>Merchant dapat mentracking consumer behavior dari setiap konsumennya dengan fitur Customer Relationship agar promosi tepat tertuju pada t arget serta dapaat meng-upgrade promosi pada mobile apps sehingga konsumen dapat langsung melihatnya.</p>
                        <div class="da-img"><img src="<?php echo $asset;?>/img/dummy/slider1.png" alt="slider1" /></div>
                    </div>
                    <div class="da-slide">
                        <h2>Easy management</h2>
                        <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p>
                        <div class="da-img"><img src="<?php echo $asset;?>/img/dummy/slider2.png" alt="slider2" /></div>
                    </div>

                    <nav class="da-arrows">
                        <span class="da-arrows-prev"></span>
                        <span class="da-arrows-next"></span>
                    </nav>
                </div>
            </div>
        </div>
        <div class="stores" id="stores">
            <div class="container">
                <h2 class="section-title black wow rubberBand">stores</h2>
                <ul>
                    <?php
                    echo "<pre>";
                    // print_r($store);
                    echo "</pre>";
                    $i=0;
                    foreach ($store as $store){
                    $i++;
                    if($i<=2){
                    ?>
                    <li class="wow fadeInLeft">
                        <?php
                }else{
                    ?>
                    <li class="wow fadeInRight">

                    <?php
                }
                ?>
                        <figure><img src="<?php echo $images;?>/store/promo/<?php echo $store->photo;?>&w=261&h=260" alt=""></figure>
                        <div class="stores-content">
                            <div class="top-content">
                                <div class="store-name "><a href="<?php echo base_url('stores/detail/'.$store->store_id);?>"><?php echo $store->store_name;?></a></div>
                                <div class="store-detail ">
                                    <div class="category">
                                        <?php echo $store->store_type;?>
                                    </div>
                                    <div class="star-rating" title="Rated 5.00 out of 5">
                                        <span style="width:85%">
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="bottom-content">
                                <div class="discount">
                                    <span><?php echo $store->disc;?></span>
                                    Available Discount
                                </div>
                                <div class="egg-visit">
                                    <span><?php echo $store->egg;?></span>
                                    Eggs Per Visit
                                </div>
                            </div>
                            <a href="#" class="view-more">See More Information</a>
                        </div>
                    </li>
                <?php }?>

            </ul>
            <a href="<?php echo base_url('stores/lists')?>" class="link-store"> see more </a>
        </div>
    </div>
    <div class="bags wow test" id="bags">
        <h2 class="section-title black wow rubberBand">bags</h2>
        <div id="component" class="component component-small fxHearbeat">
            <ul class="itemwrap">
                <li class="current"><img src="<?php echo $asset;?>/img/dummy/bags1.png" alt="bags1"/></li>
                <li><img src="<?php echo $asset;?>/img/dummy/bags2.png" alt="bags2"/></li>
                <li><img src="<?php echo $asset;?>/img/dummy/bags3.png" alt="bags3"/></li>
                <li><img src="<?php echo $asset;?>/img/dummy/bags4.png" alt="bags4"/></li>
            </ul>
            <nav class="bag-slider">
                <a class="prev" href="#">Previous item</a>
                <a class="next" href="#">Next item</a>
            </nav>
        </div>
    </div>
    <div class="ask" id="ask">
        <div class="container">
            <div class="col-left faq">
                <h3>ask</h3>
                <p>Have questions regarding Vertesac?</p>
                <p>Wanna make your brand receive higher retention?</p>
                <p>Or get many great rewards?</p>
            </div>
            <div class="col-right contact">
                <form class="contact-form" id="contact">
                    <p class="name">
                        <input name="name" id="name" type="text" class="validate[required,custom[onlyLetter],length[0,100]] feedback-input" placeholder="Name" id="name" />
                    </p>
                    <p class="email">
                        <input name="email" id="email" type="text" class="validate[required,custom[email]] feedback-input" id="email" placeholder="Email" />
                    </p>
                    <p class="text">
                        <textarea name="text" id="text" class="validate[required,length[6,300]] feedback-input" id="comment" placeholder="Comment"> </textarea>
                    </p>
                    <p class="submit">
                        <input type="submit" placeholder="submit" id="contact-submit">
                    </p>
                </form>
            </div>
        </div>
    </div>
</div>

<footer id="footer" class="footer">
    <div class="container">
        <div class="col-left footer-left">
            <div class="social">
                <a href="https://twitter.com/vertesac" target="blank_" class="twitter"></a>
                <a href="http://www.facebook.com/vertesac" target="blank_" class="facebook"></a>
            </div>
            <div class="copy">
                <p><span>Email us at:</span>info@vertesac.com</p>
                <p>Copyright @ 2014 Vertesac</p>

            </div>
        </div>
        <div class="col-right footer-right">
            <p class="extra">For user, you can download the app below</p>
            <div class="gplay">
                <img src="<?php echo $asset;?>/img/gplay.png" alt="">
            </div>
        </div>
    </div>
</footer>

<script src="<?php echo $asset;?>/js/jquery.js"></script>
<script src="<?php echo $asset;?>/js/plugins.js"></script>
<!-- // <script type="text/javascript" src="js/jquery.cslider.js"></script> -->
<script src="<?php echo $asset;?>/js/methods.js"></script>
<!-- // <script src="js/classie.js"></script> -->
<!-- // <script src="js/main.js"></script> -->
<script type="text/javascript">
function emptyForm(){
    $("#name").val("");
    $("#email").val("");
    $("#text").val("");
}
$(document).ready(function(){

    $("#contact-submit").click(function(e){
        e.preventDefault();
        if($("#email").val()==""){
            $("#email").focus();
        }else if($("#name").val()==""){
            $("#name").focus();
        }else{
            var name = $("#name").val();
            var email = $("#email").val();
            var message = $("#text").val();
            emptyForm();
            /*$('#response').html("<p>Thank you :)</p>");
            setTimeout(function(){
                $('#response').html("<p></p>");
            },5000);*/


            $.ajax({
                type: 'POST',
                url: "<?php echo base_url('contact/save');?>",
                data: {name:name,email:email,message:message},
                dataType: "json",
                success: function(response){
                    console.log(response.hasil);
                }
            });
        }
    });
});
</script>
</body>
</html>
