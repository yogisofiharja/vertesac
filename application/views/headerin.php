<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width">
    <!-- <link rel="stylesheet" type="text/css" href="css/normalize.css" /> -->
    <!-- <link rel="stylesheet" type="text/css" href="css/fxsmall.css" /> -->
    <link rel="stylesheet" href="<?php echo $asset;?>/style.css">
    <script src="<?php echo $asset;?>/js/modernizr.custom.js"></script>
    <script src="<?php echo $asset;?>/js/jquery.js"></script>
    
</head>
<body>
    <div class="gotop"></div>
    <header id="header" class="site-header">
        <div class="container">
            <div class="logo"><img src="<?php echo $asset;?>/img/dummy/logo.png" alt=""></div>
            <div class="header-right">
                <div class="searchbar">
                    <input type="search" class="search" placeholder="search">
                    <input type="submit" class="submit">
                </div>
            </div>
        </div>

    </header>
    <nav class="main-nav" style="height:auto;">
        <ul>
            <li><a href="<?php echo base_url();?>#home" class="home_nav"></a></li>
        </ul>
    </nav>