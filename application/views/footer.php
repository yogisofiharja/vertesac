
<div class="rich-footer">
            <div class="left-col">
                <h3>about</h3>
                <ul>
                    <li><a href="#">company info</a></li>
                    <li><a href="#">contact</a></li>
                    <li><a href="#">stores</a></li>
                    <li><a href="#">privacy and policy</a></li>
                </ul>
            </div>
            <div class="center-col">
                <ul>
                    <li><a href="#">company info</a></li>
                    <li><a href="#">contact</a></li>
                    <li><a href="#">stores</a></li>
                    <li><a href="#">privacy and policy</a></li>
                </ul>
            </div>
            <div class="right-col">
                <span class="copy">Copyright 2014 Vertesac All Rights Reserved.</span>
            </div>
        </div>
        <footer id="footer" class="footer">
            <div class="container">
                <div class="col-left footer-left">
                    <div class="social">
                        <a href="#" class="twitter"></a>
                        <a href="#" class="facebook"></a>
                    </div>
                    <div class="copy">
                        <p><span>Email us at:</span>info@vertesac.com</p>
                        <p>Copyright @ 2014 Vertesac</p>

                    </div>
                </div>
                <div class="col-right footer-right">
                    <p class="extra">For user, you can download the app below</p>
                    <div class="gplay">
                        <img src="<?php echo $asset;?>/img/gplay.png" alt="">
                    </div>
                </div>
            </div>
        </footer>

        
        <script src="<?php echo $asset;?>/js/plugins.js"></script>
        <!-- // <script type="text/javascript" src="js/jquery.cslider.js"></script> -->
        <script src="<?php echo $asset;?>/js/methods.js"></script>
        <!-- // <script src="js/classie.js"></script> -->
        <!-- // <script src="js/main.js"></script> -->
    </body>
</html>
