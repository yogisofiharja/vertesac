<?php 
echo "<pre>";
// print_r($store);
echo "</pre>";
?>
<?php include 'headerin.php';?>
<div id="content">
	<div class="sort">
		<div class="container">
			<label for="category">sort by</label>
			<select name="category" id="category">
				<option value="popular">popular</option>
				<option value="random">random</option>
				<option value="new">new</option>
			</select>
			<a href="#" class="sorter">Lastest Store</a>
			<a href="#" class="sorter">Top Rated Store</a>
			<a href="#" class="sorter">Most Eggs</a>
			<a href="#" class="sorter">Most Discount</a>
		</div>
	</div>
	<div class="stores-thumbnail" >
		<div class="container">
			<div class="head-section red">
				<h2 class="section-title">
					<?php 
					$index = 1;
					if(!empty($store)){
						$data = $store['0'];
						echo $data->store_type;

						?>
					</h2>
				</div>
				<ul>
					<?php foreach ($store as $toko) {
						?>
						<li class="wow">
							<figure><img src="<?php echo $images;?>/store/promo/<?php echo $toko->photo;?>&w=261&h=260" alt=""></figure>
							<div class="stores-content">
								<div class="top-content">
									<div class="store-name "><a href="<?php echo base_url('stores/detail/'.$toko->store_id);?>"><?php echo $toko->store_name;?></div>
									<div class="store-detail ">
										<div class="category">
											<?php echo $toko->store_type;?>
										</div>
										<div class="star-rating" title="Rated 5.00 out of 5">
											<span style="width:100%">
											</span>
										</div>
									</div>
								</div>
								<div class="bottom-content">
									<div class="discount">
										<span><?php echo $toko->disc;?></span>
										Available Discount
									</div>
									<div class="egg-visit">
										<span><?php echo $toko->egg;?></span>
										Eggs Per Visit
									</div>
								</div>
								<a href="#" class="view-more">See More Information</a>
							</div>
						</li>	
						<?php
							if($index%5==0){
							?>
							<?php
						}	
						 	$index++;				
					}?>			
				</ul>
				<?php 
				echo $pages;
			}else{
				echo "Tidak ada data";
			}
			?>
		</div>
	</div>
</div>
<?php include 'footer.php';?>