<?php 
echo "<pre>";
// print_r($store);
echo "</pre>";
?>
<?php include 'headerin.php';?>
<div id="content">
	<div class="store-detail">
		<div class="container">
			<!-- store basic information -->
			<figure class="product-thumb"  style="background:transparent;">
				<img src="<?php echo $images;?>/store/profile/<?php echo $store->filename;?>&w=215&h=215">
			</figure>
			<div class="detail-product">
				<div class="row">
					<div class="product-name">
						<h3><?php echo $store->name;?></h3>
						<div class="star-rating" title="Rated 5.00 out of 5">
							<?php
                            	$rating = ($store->rating/9.9999)*100;
                            ?>
							<span style="width:<?php echo $rating;?>%">
							</span>
						</div>
						<p class="quote">"<?php echo $store->slogan;?>"</p>

					</div>
					<a href="#" class="direction">get direction</a>
				</div>
				<div class="row">
					<div class="address">
						<span class="street"><?php echo $store->address;?></span>
						<span class="city"><?php echo $store->kabupaten;?></span>
						<span class="url"><a href="http://<?php echo $store->site;?>" target="_blank"><?php echo $store->site;?></a></span>
						<div class="social">
							<a href="http://www.facebook.com/<?php ?>" target="_blank" class="fb">belum</a>
							<a href="https://www.twitter.com/<?php ?>" target="_blank" class="twt">belum</a>
						</div>
						<div class="active-member"><span>82</span> Active Members</div>
						<div class="promo"><span><?php echo $store->jumlah_promo;?></span> promo</div>
					</div>
					<div class="eggs">
						<div class="available"><span><?php echo $store->egg;?></span>Available Eggs</div>
						<div class="egg-visit"><span>4</span>Eggs Per Visit</div>
					</div>
				</div>
			</div>
		</div>

	</div>
	<!-- recent promo -->
	<div class="recent-promo">
		<div class="container">
			<h3>recent promo</h3>
			<?php foreach ($recent_promo as $recent) {
			?>
			<div class="col-4"> 
				<figure><img src="<?php echo $images;?>/store/promo/<?php echo $recent->photo;?>&w=270&h=200"></figure>
				<p class="inst"><?php echo $recent->subject;?></p>
				<span class="validation">Valid From <?php echo $recent->start_time;?>-<?php echo $recent->end_time;?></span>
				<span class="kode-promo">Kode Promo     : <?php echo $recent->promo_code;?></span>
				<span class="kode-barang">Kode Barang : 02</span>

			</div>
			<?php }?>

		</div>
	</div>
	<!-- simple description about store -->
	<div class="description">
        <div class="container">
            <h3>Description</h3>
            <p><?php echo $store->description;?></p>
            <!-- <a href="#" class="more">read more</a> -->
        </div>
    </div>
    <!-- gallery of the store -->
    <div class="gallery-product">
        <div class="container">
            <h3>gallery</h3>
            <div class="gallery">
            	<?php foreach ($gallery as $gallery) {
            		# code...
            	?>
                <div class="item" style="background:transparent;"><img src="<?php echo $images;?>/store/gallery/<?php echo $gallery->filename;?>&w=347&h=320"></div>
                <?php }?>
            </div>
        </div>
    </div>
    <!-- similiar store  -->
    <div class="stores-thumbnail similiar" id="stores">
                <div class="container">
                    <div class="head-section">
                        <h2 class="section-title">similiar</h2>
                    </div>
                    <ul>
                    	<?php foreach ($similiar as $similiar) {
                    		?>

                        <li class="wow">
                            <figure><img src="<?php echo $images;?>/store/promo/<?php echo $similiar->photo;?>&w=210&h=180" alt=""></figure>
                            <div class="stores-content">
                                <div class="top-content">
                                    <div class="store-name "><a href="<?php echo base_url('store/detail/'.$similiar->store_id);?>"><?php echo $similiar->store_name;?></a></div>
                                    <div class="store-detail ">
                                        <div class="category">
                                            <?php echo $similiar->store_type;?>
                                        </div>
                                        <div class="star-rating" title="Rated 5.00 out of 5">
                                            <?php
                                            	$rate = ($similiar->rating/9.9999)*100;
                                            ?>
                                            <span style="width:<?php echo $rate;?>%">
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="bottom-content">
                                    <div class="discount">
                                        <span><?php echo $similiar->disc;?></span>
                                        Available Discount
                                    </div>
                                    <div class="egg-visit">
                                        <span><?php echo $similiar->egg;?></span>
                                        Eggs Per Visit
                                    </div>
                                </div>
                                <a href="#" class="view-more">See More Information</a>
                            </div>
                        </li> 
                        <?php 
                    	}?>                       
                    </ul>
                </div>
            </div>

</div>

<?php include 'footer.php';?>