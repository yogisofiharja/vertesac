<?php 
echo "<pre>";
// print_r($list_store);
echo "</pre>";
?>
<?php include 'headerin.php';?>
<div id="content">
	<div class="sort">
		<div class="container">
			<label for="category">sort by</label>
			<select name="category" id="category">
				<option value="popular">popular</option>
				<option value="random">random</option>
				<option value="new">new</option>
			</select>
			<a href="#" class="sorter">Lastest Store</a>
			<a href="#" class="sorter">Top Rated Store</a>
			<a href="#" class="sorter">Most Eggs</a>
			<a href="#" class="sorter">Most Discount</a>
		</div>
	</div>
	<?php foreach ($list_store as $store) {
		# code...?>
		<div class="stores-thumbnail" id="<?php echo $store['name'];?>">
			<div class="container">
				<div class="head-section red">
					<h2 class="section-title"><?php echo $store['name'];?></h2>
					<button class="button blue" onclick="location.href='<?php echo base_url('stores/type/'.$store['store_type_id']);?>'">see more</button>
				</div>
				<ul>
					<?php foreach ($store['konten'] as $toko) {
						if(count($toko)>0){						
							?>
							<li class="wow">
								<figure><img src="<?php echo $images;?>/store/promo/<?php echo $toko['photo'];?>&w=261&h=260" alt=""></figure>
								<div class="stores-content">
									<div class="top-content">
										<div class="store-name "><a href="<?php echo base_url('stores/detail/'.$toko['store_id']);?>"><?php echo $toko['store_name'];?></div>
										<div class="store-detail ">
											<div class="category">
												<?php echo $toko['store_type'];?>
											</div>
											<div class="star-rating" title="Rated 5.00 out of 5">
												<span style="width:100%">
												</span>
											</div>
										</div>
									</div>
									<div class="bottom-content">
										<div class="discount">
											<span><?php echo $toko['disc'];?></span>
											Available Discount
										</div>
										<div class="egg-visit">
											<span><?php echo $toko['egg'];?></span>
											Eggs Per Visit
										</div>
									</div>
									<a href="#" class="view-more">See More Information</a>
								</div>
							</li>	
							<?php 
						}else{
							?>
							<li><h2 class="section-title">it's empty John</h2></li>
							<?php
						}
					}?>			
				</ul>
			</div>
		</div>
		<?php
	}?>
	
</div>
<?php include 'footer.php';?>