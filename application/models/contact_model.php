<?php
class Contact_model extends CI_Model{
	var $name='';
	var $email='';
	var $message='';
	
	function save(){
		$data=array(
			'name' => $this->name,
			'email' => $this->email,
			'message' => $this->message
			);
		$this->db->insert('contact_us', $data);
		$aff_row = $this->db->affected_rows();
        if($aff_row > 0){
            $id = $this->db->insert_id();
        }else{
            $id = false;
        }

        return $id;
	}
}
?>