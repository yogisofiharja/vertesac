<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function __construct(){
        parent::__construct();
    }
	public function index()
	{
        $store = new Store_model();
		$data = array();
		$data['store'] = $store->get_empat(4);
		$data['asset'] = base_url('asset/Vertesac-html/');
		if($_SERVER["HTTP_HOST"] == "localhost") {
			$data['images'] = base_url('timthumb/timthumb.php?src=media/images');
		}else{
			$data['images'] = 'http://www.vertesac.com/timthumb/timthumb.php?src=/media/images';
			// $data['images'] = 'http://www.vertesac.com/media/images';
		}
		$this->load->view('index', $data); 
	}

	public function index_old()
	{
        $store = new Store_model();
		$data = array();
		$data['stores'] = $store->get_empat();
		// $data['asset'] = base_url('asset/Vertesac-html/');
		$this->load->view('index2', $data); 
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */