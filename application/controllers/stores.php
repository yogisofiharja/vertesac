<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Stores extends CI_Controller {

	public function __construct(){
        parent::__construct();
    }
	public function detail($id)
	{
        $store = new Store_model();
        $promo = new Promo_model();
        $gallery = new Gallery_model();
		$data = array();
		$data['store'] = $store->get_store_profile($id);
		$data['recent_promo'] = $promo->get_recent_promo($id);
		$data['gallery'] = $gallery->get_store_photo($id);
		$data['socmed'] = $store->get_store_socmed($id);
		$data['similiar'] = $store->similiar($id);
		$data['asset'] = base_url('asset/Vertesac-html/');
		if($_SERVER["HTTP_HOST"] == "localhost") {
			$data['images'] = base_url('timthumb/timthumb.php?src=media/images');
		}else{
			$data['images'] = 'http://www.vertesac.com/timthumb/timthumb.php?src=/media/images';
			// $data['images'] = 'http://www.vertesac.com/media/images';
		}
		$this->load->view('store_detail', $data); 
	}
	public function lists(){
		$store = new Store_model();
		$data['asset'] = base_url('asset/Vertesac-html/');
		if($_SERVER["HTTP_HOST"] == "localhost") {
			$data['images'] = base_url('timthumb/timthumb.php?src=media/images');
		}else{
			$data['images'] = 'http://www.vertesac.com/timthumb/timthumb.php?src=/media/images';
			// $data['images'] = 'http://www.vertesac.com/media/images';
		}
		$list_store = $store->get_store_type();
		$index_type=0;
		foreach ($list_store as $stores) {
			$temp_stores = $store->get_empata(5, $stores['store_type_id']);
			$list_store[$index_type]['konten'] = $temp_stores;
			$index_type++;
		}

		$data['list_store'] = $list_store;
		$this->load->view('stores', $data); 
	}

	public function type($store_type_id){
		$store = new Store_model();
		$data['asset'] = base_url('asset/Vertesac-html/');
		if($_SERVER["HTTP_HOST"] == "localhost") {
			$data['images'] = base_url('timthumb/timthumb.php?src=media/images');
		}else{
			$data['images'] = 'http://www.vertesac.com/timthumb/timthumb.php?src=/media/images';
			// $data['images'] = 'http://www.vertesac.com/media/images';
		}
		$this->load->library('pagination');
		$config['base_url'] = base_url('stores/type/'.$store_type_id);
		$config['use_page_numbers'] = TRUE;
		$config['per_page'] = 10; 
		$config['uri_segment'] = 4;
		// $config['use_page_numbers'] = TRUE;
		// $config['page_query_string'] = TRUE;
		$rownya = $store->count_store_type($store_type_id);
		// print_r($rownya->jumlah);exit;
		$config['total_rows'] = $rownya->jumlah;
		$this->pagination->initialize($config); 
		
		$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 1;
		$data['store'] = $store->get_by($store_type_id, $page, $config['per_page']);
		$data['pages'] = $this->pagination->create_links();
		$this->load->view('store_type', $data);
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */