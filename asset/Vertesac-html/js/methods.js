var $ = jQuery.noConflict();
$(document).ready(function(){
	$("#big-video, .video-big").fitVids();
	var $slider	= $('#da-slider');
	$(".gallery").owlCarousel({
		autoPlay: 3000, //Set AutoPlay to 3 seconds
		items : 3,
		navigation : true
	});
	// initialize the slider
	$slider.cslider();
	new WOW().init();
	// example how to go to page 3 when clicking on a link
	$('#testSlide3').on( 'click', function( event ) {
	
		$('#da-slider').cslider( 'page', 2 );
		return false;
	
	} );
	$.easing.elasout = function(x, t, b, c, d) {
		var s=1.70158;var p=0;var a=c;
		if (t==0) return b;  if ((t/=d)==1) return b+c;  if (!p) p=d*.3;
		if (a < Math.abs(c)) { a=c; var s=p/4; }
		else var s = p/(2*Math.PI) * Math.asin (c/a);
		return a*Math.pow(2,-10*t) * Math.sin( (t*d-s)*(2*Math.PI)/p ) + c + b;
	};	
	$('.about_nav').click(function(){
		event.preventDefault();
		$.scrollTo("#about", {duration:1200, easing:'elasout'});
	});
	$('.store_nav').click(function(){
		event.preventDefault();
		$.scrollTo("#stores", {duration:1200, easing:'elasout'});
	});
	$('.bags_nav').click(function(){
		event.preventDefault();
		$.scrollTo("#bags", {duration:1200, easing:'elasout'});
	});
	$('.ask_nav').click(function(){
		event.preventDefault();
		$.scrollTo("#ask", {duration:1200, easing:'elasout'});
	});


	$(window).scroll(function () {
		if ($(this).scrollTop() > 1200) {
			$('.gotop').addClass("sticky_el");
		} else {
			$(".gotop").removeClass("sticky_el");
		}
	});

	$('.gotop').click(function(){
		event.preventDefault();
		$.scrollTo("#header", {duration:1200, easing:'easeInCirc'});
	});

	(function() {
	var support = { animations : Modernizr.cssanimations },
		animEndEventNames = {
			'WebkitAnimation' : 'webkitAnimationEnd',
			'OAnimation' : 'oAnimationEnd',
			'msAnimation' : 'MSAnimationEnd',
			'animation' : 'animationend'
		},
		// animation end event name
		animEndEventName = animEndEventNames[ Modernizr.prefixed( 'animation' ) ],
		effectSel = document.getElementById( 'fxselect' ),
		component = document.getElementById( 'component' ),
		items = component.querySelector( 'ul.itemwrap' ).children,
		current = 0,
		itemsCount = items.length,
		nav = component.querySelector( 'nav' ),
		navNext = nav.querySelector( '.next' ),
		navPrev = nav.querySelector( '.prev' ),
		isAnimating = false;

	function init() {
		navNext.addEventListener( 'click', function( ev ) { ev.preventDefault(); navigate( 'next' ); } );
		navPrev.addEventListener( 'click', function( ev ) { ev.preventDefault(); navigate( 'prev' ); } );
	}


	function navigate( dir ) {
		var cntAnims = 0;


		var currentItem = items[ current ];

		if( dir === 'next' ) {
			current = current < itemsCount - 1 ? current + 1 : 0;
		}
		else if( dir === 'prev' ) {
			current = current > 0 ? current - 1 : itemsCount - 1;
		}

		var nextItem = items[ current ];

		var onEndAnimationCurrentItem = function() {
			this.removeEventListener( animEndEventName, onEndAnimationCurrentItem );
			classie.removeClass( this, 'current' );
			classie.removeClass( this, dir === 'next' ? 'navOutNext' : 'navOutPrev' );
			++cntAnims;
			if( cntAnims === 2 ) {
				isAnimating = false;
			}
		}

		var onEndAnimationNextItem = function() {
			this.removeEventListener( animEndEventName, onEndAnimationNextItem );
			classie.addClass( this, 'current' );
			classie.removeClass( this, dir === 'next' ? 'navInNext' : 'navInPrev' );
			++cntAnims;
			if( cntAnims === 2 ) {
				isAnimating = false;
			}
		}

		if( support.animations ) {
			currentItem.addEventListener( animEndEventName, onEndAnimationCurrentItem );
			nextItem.addEventListener( animEndEventName, onEndAnimationNextItem );
		}
		else {
			onEndAnimationCurrentItem();
			onEndAnimationNextItem();
		}

		classie.addClass( currentItem, dir === 'next' ? 'navOutNext' : 'navOutPrev' );
		classie.addClass( nextItem, dir === 'next' ? 'navInNext' : 'navInPrev' );
	}

	init();
	})();


	$('.login-wrap').css({
		'height':($(document).height()+'px')
	});
	$('.login-wrap .container').css({
		'height':($(window).height()+'px')
	});
	$('.login_nav').click(function(event) {
		$('.login-wrap').fadeToggle();
	});

	
	$('.login-wrap').click(function() {
		$(this).fadeOut();
		});
		$('.login-form').click(function(event){
		    event.stopPropagation();
	});
	
});
